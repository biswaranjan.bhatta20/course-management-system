# Course Registration System

### Problem Statement 

Enrolling Students to the course is very difficult , critical and important process.The Existing Manual system is prone to erros and  We  have developed this course registration system to replace the existing manual registration since manual system are prone to  errors  and take more time.

Students can easily register the course in their system without any difficulty and can easily understand  and also time taken for registration  is  less  when compared to manual registration. Options are given to the student to select their course.Instructors can add & evaluate  assignments without any hassels.

### Project Overview 

*  Admin can Login to view the details of students, Courses and Assignments 
*  Student can login view his course and view / submit assignments 
*  Course Registration System has UI and provides login screen for respective user (Admin/Instructor/Student)


## Modules 

### Admin

* Approves Student Registration
* Unregister Student
* Add/delete Courses 
* Add Assignments 
* View student Submitted Assignments 

### Instructor

* Logins to Assigned Course 
* View Students Info
* Add/delete Assignments 
* View student Submitted Assignments 

###  Student 

* Register to particular Course
*	Logs in using Credentials 
*	View  Particular Course
*	View and Submit assignments 

### System Design

### Use Case Diagram

``` plantuml 

@startuml
left to right direction
actor Student
actor Admin


rectangle StudentInfo {
  usecase "Approve student registration" as UC1
  usecase "Delete student registration" as UC2
}

rectangle Course {
  usecase "Add Course" as UC3
  usecase "View Course" as UC4
  usecase "Delete Course" as UC5
  usecase "View students enrolled in Course" as UC6

}
rectangle Assignment {
  usecase "Add Assignment" as UC7
  usecase "View Assignment" as UC8
  usecase "Delete Submitted Assignments" as UC9
  usecase "Submit Assignments" as UC10

}
rectangle Institute {
  usecase "Login" as UC11
  usecase "Register" as UC12

 

}


Admin --> (UC1)
Admin --> (UC2)
Admin --> (UC3)
Admin --> (UC5)
Admin --> (UC6)
Admin --> (UC7)
Admin --> (UC8)
Admin --> (UC9)
Admin --> (UC10)
Admin --> (UC11)

Student --> (UC8)
Student --> (UC10)
Student --> (UC11)
Student --> (UC12)

@enduml

```
### Class Diagrams 

``` plantuml

@startuml
class Institute{
    - Admin
    - studentInfo[] list<Student>
    - Courses[] list<Course>
   
    + viewCourseInfo()
    + viewStudentInfo()
    + registerStudent()
    + unregisterStudent()
  }

  class Admin{
    + registerStudent()
    + unregisterStudent()
    + addCourse()
    + deleteCourse()
    + viewstudentsenrolledCourse()
    + addAssignment()
    + viewsubmittedAssignments()
    + deleteAssignments()
  }

  class Course {
    - courseId
    - courseName
    - duration
    
    + getCourseInfo()
    + viewStudentsenrolledCourse()
  
  }

  class Student {
    - studentID
    - name
    - Course

    + getCourseID()
    + submitAssignments()
    
  }

class Assignment {
    - AssignmentID
    - CourseID
  
    + getCourseID()
    + getAssignmentInfo()
    + submitAssignments()
     
  }

Institute "1" o-- "1" Course : contains >
Course "1" o-- "*" Assignment : contains >

Admin "1" .. "*" Student : adds >
Admin "1" .. "*"  Course : adds >
Admin "1" .. "*"  Assignment : adds >
Student "*" -- "1" Course : enrolls >

Student .. Assignment : Submits  >
@enduml

```
## Sequence Diagrams 
#### Initial Application

```plantuml

@startuml
actor user

user -> Admin : Admin Logins
@enduml

```
#### Admin Panel

``` plantuml

@startuml
Admin ---------> Institute: Logs in
activate Institute

== Adding course  ==
Admin --->Course : Adds/Edit Course
activate Course

Admin <--- Course : Delete Course
deactivate Course

== Adding Assignment  ==

Admin ---> Assignment : createAssignment 
activate Assignment

Admin ---> Assignment : View Student Submitted Assignment 

Admin <--- Assignment : Delete Assignment 
deactivate Assignment

== Adding Student  ==
Admin ---> Student : Adds Students view students enrolled in course and Assigments 
activate Student

Admin <--- Student : Delete Students
deactivate Student

Admin <------- Institute: Logout
deactivate Institute
@enduml

```
#### Student Panel

``` plantuml

@startuml
Student --> Institute : Logs in 
Student --> Course : view the courses and enrolls 
Student ---> Assignment : SubmitAssignments for that particular course
Student <-- Institute : Logs out
@enduml

```

## Project

#### Admin Login

``` plantuml
@startsalt
{+
  UserName  | "Admin   "
  Password | "****     "
  [Submit] | [Cancel]
}
@endsalt

```
#### Admin Home Page 

``` plantuml
@startsalt
{+
{* Course| Assignments | Student }
{
Admin Home
}
          [AddCourses]

}
@endsalt

```
#### Add Course

``` plantuml

@startsalt
{+
  CourseId  | "ABCFS01   "
  Name | "    "
  Duration | "    "
  [Submit] | [Cancel]
}
@endsalt

```

#### Courses 

``` plantuml
@startsalt
{+
{* Course| Assignments | Student 
Course | AddCourse | ViewCourse
}

{

Admin Home
}
         

}
@endsalt

```
#### View Course 

``` plantuml

@startsalt
{#
courseId | courseName | duration | .  |.
ABCJ123 | Java | 3 months  | Edit | Delete
ABCJFS124 | Java Full Stack | 6 months  | Edit | Delete
ABCDS125 | Data Science | 6 months | Edit | Delete
ABCT126 | Testing | 2 months | Edit | Delete
ABCCS127 | CyberSecurity | 3 months | Edit | Delete

}
@endsalt

```
#### Assignments 

``` plantuml
@startsalt
{+
{* Course| Assignments | Student 
Assignments| AddAssignments | ViewAssignments
}
{
Admin Home
}
}
@endsalt

```

#### Add Assignments

``` plantuml

@startsalt
{+
  AssignmentId  | "Assign01   "
   Course|  ^Select Course ^
   Topic | "     "
  [Add] | [Cancel]
}
@endsalt

```
#### Assignments 

``` plantuml

@startsalt
{#
AssignId | Course | Topic | .  |.
Assign01 | Java | Inheritance  | Edit | Delete
Assign02 | Java Full Stack | Webpage  | Edit | Delete
Assign03 | DataScience | Analysis  | Edit | Delete
Assign04 | Testing | Selenium | Edit | Delete

}
@endsalt

```
#### Student 

``` plantuml
@startsalt
{+
{* Course| Assignments | Student 
Student| Register | ViewDetails | ViewAssignments 
}

{

Admin Home
}
        
}
@endsalt

```
#### Student Registration

``` plantuml
@startsalt
{+
  StudentId  | "ABCSTDJ01   "
  Name | "    "
  Address| "    "
  Phone | "     "
  AdharNo | "     "
  SelectCourse | ^Select Course ^
  [Submit] | [Cancel]
}
@endsalt

```
#### Student Details 

``` plantuml

@startsalt
{#
StudentId | Name | Address | Phone  |Adharno | Course | . | .
ABCSTDJ01 | Saarika | Bangalore  | 98088 00000 | 1234 8904 1230 1232 | JAVA | Edit | Delete
ABCSTDJFS02 | Pooja | Basveshwar nagar  | 98040 42322 | 8971 2433 2833 1233 | JAVA FULL STACK | Edit | Delete
ABCSTDDS03 | Khushi | Vijayanagar  | 98008 34234 | 8475 4568 4853 3475 | DATA SCIENCE | Edit | Delete 
ABCSTDCS04 | Aarya | Rajajinagar  | 96088 34234 | 6475 5658 4323 4750 | CYBER SECURITY | Edit | Delete 

}
@endsalt

```
#### Student Login 

``` plantuml

@startsalt
{+
  Login Id    | "ABCSTDJFS01"
  Password | "********     "
  [Submit] | [Cancel]
}
@endsalt
```
#### Student Home 

``` plantuml

@startsalt
{+
{* Course| Assignments }
{
Student Home
}
        
}
@endsalt


```
#### Student Course details 

``` plantuml

@startsalt
{+
{* Course| Assignments 
Course| Coursedetails
}

{

Student Home
}
         
}
@endsalt

```

#### Student Course details 

``` plantuml

@startsalt
{#
CourseId | CourseName | Duration 
ABCJFS124 | Java Full Stack | 6 months  
}
@endsalt

```
#### Assignments List

``` plantuml
@startsalt
{#
AssignId | Course | Topic |.
Assign02 | Java Full Stack | Webpage  | View/Submit
Assign03 | Java Full Stack | JavaProgram  | View/Submit

}
@endsalt
```

